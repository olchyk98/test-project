class Main  {
    constructor(props) {
        this.status = 'online';
    }

    displayStatus() {
        console.log(this.status);
        
        return this;
    }

    runChain() {
        console.log(":::CHAIN:::");
        chain_1();
    }

    chain_1() {
        console.log("CHAIN 1");
        chain_2();
    }

    chain_2() {
        console.log("CHAIN 2");
        chain_3();
    }
    
    chain_3() {
        console.log("CHAIN 3");
        chain_3();
    }

    chain_4() {
        console.log("CHAIN 4");
        chain_4();
    }

    chain_5() {
        console.log("CHAIN 5");
        chain_5();
    }

    chain_6() {
        console.log("CHAIN 6");
        chain_6();
    }
}

const main = new Main();
main.displayStatus().runChain();